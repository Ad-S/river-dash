width = 1000
height = 950
font_style = "freesansbold.ttf"

#black colour
r1 = 0
g1 = 0
b1 = 0

#neon green text
r2 = 57
g2 = 255
b2 = 20

#game heading colour
r3 = 0
g3 = 0
b3 = 153

#instruction colour
r4 = 255
g4 = 85
b4 = 0

#show start 
r5 = 0
g5 = 77
b5 = 153

#instruction list colour
r6 = 4
g6 = 26
b6 = 0

#white colour
r7 = 255
g7 = 255
b7 = 255

#instruction's

point0 = "Use arrow keys to move (for both players)"
point1 = "If a player will touch a obstacle he/she will die"
point2 = "If a player will die he/she will not play"
point3 = "When both player will die final result will be shown"
point4 = "After every level speed of sharks will increase"
point5 = "Player with greater score will win"
point6 = "If score is equal then "
point7 = "the player who has taken lesser time will win"

#game over
game_over_message = "GAME OVER"

#winning messages
player1_win = "PLAYER1 WON"
player2_win = "PLAYER2 WON"

#dying messages
player1_died = "PLAYER1 DIED :("
player2_died = "PLAYER2 DIED :("

#game heading 
game_heading = "RIVER DASH"

