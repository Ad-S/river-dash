# RIVER DASH INSTRUCTION MANUAL  

## How to load the game  

Go to the terminal and by using the command :-  python3 main.py  

## RULES

1) Use arrow keys to move (for both players) (as only one player will play at a time).  
2) If a player will touch a obstacle he/she will die.  
3) If a player will die he/she will not play.  
4) When both player will die final result will be shown.  
5) After every level speed of sharks will increase.  
6) Player with greater score will win.  
7) If score is equal then the player who has taken lesser time to score that much score will win.  
8) If score and time both is equal then player1 will win (this condition is rare) (as player1 is starting he gets this benefit).  

## To start the game press enter  
