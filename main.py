import math
import logging
import threading
import time
from threading import Timer
import pygame
from config import *

pygame.init()

# setting the game window size
screen = pygame.display.set_mode((width, height))

# writing the caption of the game
pygame.display.set_caption("RIVER DASH")

# to make the timer using clock
clock = pygame.time.Clock()
timer1 = 0
timer2 = 0
count1 = 0
count2 = 0
constant = 1

# to check whether player 1 or player 2 is dead or not
player1_dead = 0
player2_dead = 0

# only active player will be on the screen
player_active = 1
player2_active = 0

# placing player 1 in the game
playerImg = pygame.image.load('people.png')
playerX = 64
playerY = height - 64
x_change = 0
y_change = 0

# placing player 2 in the game
player2Img = pygame.image.load('woman.png')
player2X = width - 64
player2Y = 50
x_change = 0
y_change = 0

# making list for fix object
fix_obsImg = []
fix_obsX = []
fix_obsY = []
fix_num = 6

# now placing 6 fix objects at different positions
for i in range(fix_num):
    fix_obsImg.append(pygame.image.load('stone.png'))

fix_obsX.append(12)
fix_obsY.append(768)

fix_obsX.append(390)
fix_obsY.append(768)

fix_obsX.append(890)
fix_obsY.append(768)

fix_obsX.append(90)
fix_obsY.append(168)

fix_obsX.append(500)
fix_obsY.append(168)

fix_obsX.append(780)
fix_obsY.append(168)

# making list for moving object
mov_obsImg = []
mov_obsX = []
mov_obsY = []
x_movobs_change = []
mov_num = 4

# now intializing intial position and speed of moving objects
mov_obsImg.append(pygame.image.load('shark.png'))
mov_obsX.append(120)
mov_obsY.append(568)
x_movobs_change.append(4)

mov_obsImg.append(pygame.image.load('shark.png'))
mov_obsX.append(880)
mov_obsY.append(568)
x_movobs_change.append(8)

mov_obsImg.append(pygame.image.load('shark.png'))
mov_obsX.append(300)
mov_obsY.append(368)
x_movobs_change.append(4)

mov_obsImg.append(pygame.image.load('shark.png'))
mov_obsX.append(700)
mov_obsY.append(368)
x_movobs_change.append(-8)

# doing this to check whether points for passing a obstacle
# has been counted or not
player1_fix1 = 0
player1_fix2 = 0
player1_mov1 = 0
player1_mov2 = 0

player2_fix1 = 0
player2_fix2 = 0
player2_mov1 = 0
player2_mov2 = 0

# loading the platform image
platformImg = pygame.image.load('platform.jpg')

# intializing the score of player 1 and its position
score_value = 0
score1_flag = 1
font = pygame.font.Font(font_style, 32)

score_placeX = 0
score_placeY = 0

# intializing the score of player 2 and its position
score2_value = 0
score2_flag = 1
font = pygame.font.Font(font_style, 32)

score2_placeX = 400
score2_placeY = 0

# Game over text
over_font = pygame.font.Font(font_style, 64)

# player1 winning text
player1_win_font = pygame.font.Font(font_style, 64)

# player2 winning text
player2_win_font = pygame.font.Font(font_style, 64)

# player1 died text
player1_died_font = pygame.font.Font(font_style, 64)

# player2 died text
player2_died_font = pygame.font.Font(font_style, 64)

# player1 lvl passing text
player1_lvl_passed_font = pygame.font.Font(font_style, 64)

# player2 lvl passing text
player2_lvl_passed_font = pygame.font.Font(font_style, 64)

# home page font
home_font1 = pygame.font.Font(font_style, 64)
home_font2 = pygame.font.Font(font_style, 32)

# level
player1_on_lvl = 1
player2_on_lvl = 1

player1_dead = 0
player2_dead = 0

player1_lvl = 1
player2_lvl = 1

# defining all the function
# what they do is clear from their name


def inc_timer1():
    global timer1
    timer1 += 1


def inc_timer2():
    global timer2
    timer2 += 1


def show_score(x, y):
    score = font.render("Player 1 : " + str(score_value), True, (r2, g2, b2))
    screen.blit(score, (x, y))


def show_score2(x, y):
    score = font.render("Player 2 : " + str(score2_value), True, (r2, g2, b2))
    screen.blit(score, (x, y))


def show_timer1(x, y):
    value = font.render("Timer 1 : " + str(timer1), True, (r2, g2, b2))
    screen.blit(value, (x, y))


def show_timer2(x, y):
    value = font.render("Timer 2 : " + str(timer2), True, (r2, g2, b2))
    screen.blit(value, (x, y))


def player(x, y):
    screen.blit(playerImg, (x, y))


def player2(x, y):
    screen.blit(player2Img, (x, y))


def mov_obs(x, y, i):
    screen.blit(mov_obsImg[i], (x, y))


def game_over_text():
    over_text = over_font.render(game_over_message, True, (r2, g2, b2))
    screen.blit(over_text, (300, 50))


def show_winner1():
    win_text = player1_win_font.render(player1_win, True, (r2, g2, b2))
    screen.blit(win_text, (275, 200))


def show_winner2():
    win_text = player2_win_font.render(player2_win, True, (r2, g2, b2))
    screen.blit(win_text, (275, 200))


def show_player1_died():
    died_text = player1_died_font.render(player1_died, True, (r2, g2, b2))
    screen.blit(died_text, (275, 400))


def show_player2_died():
    died_text = player2_died_font.render(player2_died, True, (r2, g2, b2))
    screen.blit(died_text, (275, 400))


def show_Heading():
    title = home_font1.render(game_heading, True, (r3, g3, b3))
    screen.blit(title, (270, 50))


def show_Instructions():
    instruction = home_font1.render("INSTRUCTIONS :", True, (r4, g4, b4))
    screen.blit(instruction, (0, 200))


def show_start():
    instruction = home_font1.render("Press ENTER to start", True, (r5, g5, b5))
    screen.blit(instruction, (200, 750))


def show_instructions_list(x, y_position):
    content = home_font2.render("--> " + str(x), True, (r6, g6, b6))
    screen.blit(content, (0, y_position))


def show_player1_lvl_passed():
    v1 = player1_lvl_passed_font
    lvl = v1.render("Player1 passed level " +
                    str(player1_on_lvl), True, (r4, g4, b4))
    screen.blit(lvl, (150, 400))


def show_player2_lvl_passed():
    v1 = player2_lvl_passed_font
    lvl = v1.render("Player2 passed level " +
                    str(player2_on_lvl), True, (r4, g4, b4))
    screen.blit(lvl, (150, 400))


def show_final_score1():
    score = font.render("Player 1 : " + str(score_value), True, (r1, g1, b1))
    screen.blit(score, (350, 500))


def show_final_time1():
    total_time = font.render("Time taken by Player 1 : " +
                             str(timer1) + " sec", True, (r1, g1, b1))
    screen.blit(total_time, (350, 550))


def show_final_score2():
    score = font.render("Player 2 : " + str(score2_value), True, (r1, g1, b1))
    screen.blit(score, (350, 600))


def show_final_time2():
    total_time = font.render("Time taken by Player 2 : " +
                             str(timer2) + " sec", True, (r1, g1, b1))
    screen.blit(total_time, (350, 650))

# condition for collision


def isCollision(playerX, playerY, mov_obsX, mov_obsY):
    dist = math.sqrt((math.pow(playerX - mov_obsX, 2)) +
                     (math.pow(playerY - mov_obsY, 2)))
    if dist < 64:
        return True
    else:
        return False


# at the start home page will be shown
home_page_show = 1

working = True

while working:

    while home_page_show == 1:
        screen.fill((r7, g7, b7))
        show_Heading()
        show_Instructions()
        show_instructions_list(point0, 300)  # placing intruction's on
        show_instructions_list(point1, 350)  # the home page
        show_instructions_list(point2, 400)
        show_instructions_list(point3, 450)
        show_instructions_list(point4, 500)
        show_instructions_list(point5, 550)
        show_instructions_list(point6, 600)
        show_instructions_list(point7, 650)
        show_start()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                working = False
                home_page_show = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    home_page_show = 0  # if enter pressed game will start
        pygame.display.update()

    # checking whether both are dead or not
    if player1_dead == 0 or player2_dead == 0:
        screen.fill((20, 20, 80))
        screen.blit(platformImg, (0, 850))
        screen.blit(platformImg, (0, 50))
        screen.blit(platformImg, (0, 250))
        screen.blit(platformImg, (0, 450))
        screen.blit(platformImg, (0, 650))

        screen.blit(fix_obsImg[0], (12, 768))
        screen.blit(fix_obsImg[1], (390, 768))
        screen.blit(fix_obsImg[2], (890, 768))
        screen.blit(fix_obsImg[3], (90, 168))
        screen.blit(fix_obsImg[4], (500, 168))
        screen.blit(fix_obsImg[5], (780, 168))

    # on pressing cross QUIT
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            working = False

        # setting up controls
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                x_change = -5
            if event.key == pygame.K_RIGHT:
                x_change = 5
            if event.key == pygame.K_UP:
                y_change = -5
            if event.key == pygame.K_DOWN:
                y_change = 5

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                x_change = 0
                y_change = 0
            if event.key == event.key == pygame.K_RIGHT:
                x_change = 0
                y_change = 0
            if event.key == event.key == pygame.K_UP:
                x_change = 0
                y_change = 0
            if event.key == event.key == pygame.K_DOWN:
                x_change = 0
                y_change = 0

    # when player1 is playing
    if player_active == 1:

        # will increase timer after 1 sec
        count1 += 1
        timer1 = math.floor(count1 / 80)

        playerX += x_change
        playerY += y_change

        # conditions so that player don't leave the screen
        if playerX <= 0:
            playerX = 0
        elif playerX >= width - 64:
            playerX = width - 64

        # if player 1 wins the round
        if playerY <= 50:
            playerY = height - 64
            playerX = 0
            screen.fill((r1, g1, b1))
            show_player1_lvl_passed()
            pygame.display.update()
            pygame.time.wait(1000)
            if player2_dead == 0:
                player_active = 0
                player2_active = 1
            player1_lvl += 0.5
            player1_on_lvl += 1
            player1_fix1 = 0
            player1_fix2 = 0
            player1_mov1 = 0
            player1_mov2 = 0
        elif playerY >= height - 64:
            playerY = height - 64

        # increasing score on passing obstacle
        if playerY <= height - 200 - 64:
            if player1_fix1 == 0:
                score_value += 5
                player1_fix1 = 1
        if playerY <= height - 400 - 64:
            if player1_mov1 == 0:
                score_value += 10
                player1_mov1 = 1
        if playerY <= height - 600 - 64:
            if player1_mov2 == 0:
                score_value += 10
                player1_mov2 = 1
        if playerY <= height - 800 - 64:
            if player1_fix2 == 0:
                score_value += 5
                player1_fix2 = 1

        # increasing obstacle speed
        for i in range(mov_num):

            mov_obsX[i] += x_movobs_change[i]

            if mov_obsX[i] <= 0:
                mov_obsX[i] = 0
                if i == 1 or i == 3:
                    x_movobs_change[i] = 8 * player1_lvl
                elif i == 0 or i == 2:
                    x_movobs_change[i] = 4 * player1_lvl

            elif mov_obsX[i] >= width - 64:
                mov_obsX[i] = width - 64
                if i == 1 or i == 3:
                    x_movobs_change[i] = -8 * player1_lvl
                elif i == 0 or i == 2:
                    x_movobs_change[i] = -4 * player1_lvl

            mov_obs(mov_obsX[i], mov_obsY[i], i)

        player(playerX, playerY)
        show_timer1(750, 0)

    # collision condition's
    for i in range(mov_num):

        collided = isCollision(playerX, playerY, mov_obsX[i], mov_obsY[i])
        if collided:
            screen.fill((r1, g1, b1))
            show_player1_died()
            pygame.display.update()
            pygame.time.wait(1000)
            pygame.display.update()
            player1_dead = 1
            player_active = 0
            if player2_dead == 0:
                player2_active = 1
            playerY = height - 64
            playerX = 0

        collided2 = isCollision(player2X, player2Y, mov_obsX[i], mov_obsY[i])
        if collided2:
            screen.fill((r1, g1, b1))
            show_player2_died()
            pygame.display.update()
            pygame.time.wait(1000)
            player2_dead = 1
            if player1_dead == 0:
                player_active = 1
            player2_active = 0
            player2Y = 50
            player2X = width - 64

    for i in range(fix_num):
        collided = isCollision(playerX, playerY, fix_obsX[i], fix_obsY[i])
        if collided:
            screen.fill((r1, g1, b1))
            show_player1_died()
            pygame.display.update()
            pygame.time.wait(1000)
            player1_dead = 1
            player_active = 0
            if player2_dead == 0:
                player2_active = 1
            playerY = height - 64
            playerX = 0

    for i in range(fix_num):
        collided2 = isCollision(player2X, player2Y, fix_obsX[i], fix_obsY[i])
        if collided2:
            screen.fill((r1, g1, b1))
            show_player2_died()
            pygame.display.update()
            pygame.time.wait(1000)
            player2_dead = 1
            if player1_dead == 0:
                player_active = 1
            player2_active = 0
            player2Y = 50
            player2X = width - 64

    # when player2 is playing
    if player2_active == 1:

        # will increase timer after 1 sec
        count2 += 1
        timer2 = math.floor(count2 / 80)

        player2X += x_change
        player2Y += y_change

        # conditions so that player don't leave the screen
        if player2X <= 0:
            player2X = 0
        elif player2X >= width - 64:
            player2X = width - 64

        # if player 2 wins the round
        if player2Y >= height - 64:
            player2X = width - 64
            player2Y = 50
            screen.fill((r1, g1, b1))
            show_player2_lvl_passed()
            pygame.display.update()
            pygame.time.wait(1000)
            if player1_dead == 0:
                player_active = 1
                player2_active = 0
            player2_lvl += 0.5
            player2_on_lvl += 1
            player2_fix1 = 0
            player2_fix2 = 0
            player2_mov1 = 0
            player2_mov2 = 0
        elif player2Y <= 50:
            player2Y = 50

        # increasing score on passing obstacle
        if player2Y >= 250:
            if player2_fix1 == 0:
                score2_value += 5
                player2_fix1 = 1
        if player2Y >= 450:
            if player2_mov1 == 0:
                score2_value += 10
                player2_mov1 = 1
        if player2Y >= 650:
            if player2_mov2 == 0:
                score2_value += 10
                player2_mov2 = 1
        if player2Y >= 850:
            if player2_fix2 == 0:
                score2_value += 5
                player2_fix2 = 1

        # increasing obstacle speed
        for i in range(mov_num):

            mov_obsX[i] += x_movobs_change[i]

            if mov_obsX[i] <= 0:
                mov_obsX[i] = 0
                if i == 1 or i == 3:
                    x_movobs_change[i] = 8 * player2_lvl
                elif i == 0 or i == 2:
                    x_movobs_change[i] = 4 * player2_lvl

            elif mov_obsX[i] >= width - 64:
                mov_obsX[i] = width - 64
                if i == 1 or i == 3:
                    x_movobs_change[i] = -8 * player2_lvl
                elif i == 0 or i == 2:
                    x_movobs_change[i] = -4 * player2_lvl

            mov_obs(mov_obsX[i], mov_obsY[i], i)

        player2(player2X, player2Y)
        show_timer2(750, 0)

    # if any player is alive show score on top of screen
    if player1_dead == 0 or player2_dead == 0:
        show_score(score_placeX, score_placeY)
        show_score2(score2_placeX, score2_placeY)
    pygame.display.update()
    clock.tick(80)  # setting fps as 80

    # creating game over screen
    if player1_dead == 1 and player2_dead == 1:
        screen.fill((r7, g7, b7))
        game_over_text()
        if score_value > score2_value:
            show_winner1()
        elif score_value < score2_value:
            show_winner2()
        elif timer1 > timer2:
            show_winner2()
        elif timer1 <= timer2:
            show_winner1()
        show_final_score1()
        show_final_time1()
        show_final_score2()
        show_final_time2()

        pygame.display.update()
